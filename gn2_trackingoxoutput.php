<?php
/*
 * GN2_Tracking - Universal Statistic Module
 * @copyright GN2 netwerk
 * @link http://www.gn2-netwerk.de/
 * @author Christoph Stäblein
 * @author Dave Holloway
 * @version   SVN: $Id: gn2_tracking_oxoutput.php 45 2011-11-10 12:18:09Z dh $
 */

$dir = dirname(__FILE__);
require_once $dir . '/bootstrap.inc.php';

class GN2_TrackingOxOutput extends GN2_TrackingOxOutput_parent
{

    public function process($sValue, $sClassName)
    {
        $sValue = parent::process($sValue, $sClassName);

        if(!isAdmin())
        {
            $sValue = $this->gn2_tracking_outputFilter($sValue);
        }

        return $sValue;
    }

    public function gn2_tracking_outputFilter($output)
    {
        $output = GN2_Tracking::replaceOutput($output);

        $cl =  oxRegistry::getConfig()->getRequestParameter('cl');
        if ($cl == 'shop_system') {
            $version = '<p style="color:#777777">'
                       .'GN2_Tracking Version: '.file_get_contents(dirname(__FILE__).'/version.php').'</p>';
            if (isset($_POST['save'])) {
                $output = str_replace(
                    'myedit.fnc.value=\'save\'"" >',
                    'myedit.fnc.value=\'save\'"" >'.$version,
                    $output
                );
            }
        }
        return $output;
    }
}
