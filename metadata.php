<?php

/**
 * Module information
 */

$link = 'https://bitbucket.org/gn2netwerk/gn2_tracking/src/master/INSTALL.pdf';
$aModule = array(
    'id' => 'gn2_tracking',
    'title' => 'gn2 :: Tracking',
    'description' => 'gn2 tracking takes away the pain of adding all the Google
                      Snippets to your site. Just upload to /modules/gn2_tracking, add your
                      UA to settings.ini, activate and add your conversion funnel in
                      Google Analytics (see <a style="text-decoration:underline;" href="'.$link.'">INSTALL.pdf</a>). Done.',
    'thumbnail' => 'gn2_tracking.png',
    'version' => '1.1',
    'author' => 'gn2 netwerk',
    'url' => 'http://www.gn2-netwerk.de',
    'email' => 'kontakt@gn2-netwerk.de',
    'extend' => array(
        'oxoutput' => 'gn2_tracking/gn2_trackingoxoutput',
    )
);
