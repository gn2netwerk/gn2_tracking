<?php
/*
 * @package gn2_tracking
 */
/*
Plugin Name: GN2_Tracking
Plugin URI: http://www.gn2-netwerk.de
Description: Universal Statistics Addon
Version: 0.1
Author: Christoph Stäblein, Dave Holloway
Author URI: http://www.gn2-netwerk.de
*/

$dir = ABSPATH . 'wp-content/plugins/gn2_tracking/';
include_once($dir . "bootstrap.inc.php");

ob_start('gn2_tracking_outputFilter');
register_shutdown_function('ob_end_flush');
function gn2_tracking_outputFilter($output)
{
    $output = GN2_Tracking::replaceOutput($output);
    return $output;
}