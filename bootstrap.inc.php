<?php
/**
 * GN2_Tracking
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage GN2_Tracking
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
require_once $dir . '/classes/class.gn2_tracking.inc.php';
GN2_Tracking::$dir = $dir;
GN2_Tracking::identifyEnvironment();
GN2_Tracking::$snippets = GN2_Tracking::generateSnippets();
