<?php
/**
 * GN2_Tracking_Service_GoogleAdwords
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage Service_GoogleAdwords
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @author     Steve Knornschild <knornschild@sitzdesign.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
class GN2_Tracking_Service_GoogleAdwords extends GN2_Tracking_Environment
{

    /**
     * Constructor
     * Set ini settings into class variable
     */
    public function __construct()
    {
        $this->data = parse_ini_file(dirname(__FILE__) . '/../gn2_tracking.ini', true);
    }


    /**
     * main function to generate the snippet
     *
     * @return string
     */
    public function generateCode()
    {
        if (!$this->isBackend()) {

            $show_code = false;
            $code = "";
            $google_tag_var = "";
            $log_conversion = false;
            $order_total = 0;

            // Lade benutzerdefinierte Variablen für Remarketing Tracking
            if ($this->isShop() && $this->hasProducts()) {
                $oProducts = $this->getProducts();

                if (count($oProducts) > 0) {
                    $sEcommPageType = $this->getPagetype();
                    $sListProdId = '';
                    $sListPrices = '';

                    foreach($oProducts as $oProduct) {
                        if ($sListProdId != "") { $sListProdId .= ","; }
                        $sListProdId .= '"' . $oProduct["artNr"]. '"';

                        if ($sListPrices != "") { $sListPrices .= ","; }
                        $sListPrices .= '' . $oProduct["price"]. '';
                    }

                    $show_code = true;
                    $google_tag_var = 'google_tag_params';

                    $code .= "\n";
                    $code .= '<script type="text/javascript">' . "\n";
                    $code .= 'var '.$google_tag_var.' = {' . " \n";
                    $code .= 'ecomm_prodid: [' . $sListProdId . '],'. " \n";
                    $code .= 'ecomm_pagetype: "' . $sEcommPageType . '",'. " \n";
                    $code .= 'ecomm_totalvalue: [' . $sListPrices . '],'. " \n";
                    $code .= '};'. " \n";
                    $code .= '</script>' . "\n" . " \n";
                }
            }

            // Lade Gesamtpreis für Conversion Tracking
            if ($this->isShop() && $this->isLastStep()) {
                $order = $this->getOrder();
                $order_total = $order['OrderTotal'];
                $log_conversion = true;
                $show_code = true;
            }

            if ($show_code) {
                $code .= "\n";
                $code .= '<script type="text/javascript">' . "\n";
                $code .= '/* <![CDATA[ */' . "\n";
                $code .= 'var google_conversion_id = ' . $this->data['settings']['adwords_conversion_id'] . ';' . "\n";

                if ($log_conversion) {
                    $code .= 'var google_conversion_language = "' . $this->data['settings']['adwords_conversion_language'] . '";' . "\n";
                    $code .= 'var google_conversion_format = "' . $this->data['settings']['adwords_conversion_format'] . '";' . "\n";
                    $code .= 'var google_conversion_color = "' . $this->data['settings']['adwords_conversion_color'] . '";' . "\n";
                    $code .= 'var google_conversion_label = "' . $this->data['settings']['adwords_conversion_label'] . '";' . "\n";
                    $code .= 'var google_conversion_value = ' . $order_total . ';' . "\n";
                    $code .= 'var google_remarketing_only = false;' . "\n";
                } else {
                    $code .= 'var google_remarketing_only = true;' . "\n";
                }

                // Benutzerdefinierte Remarketing-Parameter übergeben
                if ($google_tag_var != "") {
                    $code .= 'var google_custom_params = window.' . $google_tag_var . ';' . "\n";
                }

                $code .= '/* ]]> */' . "\n";
                $code .= '</script>' . "\n";
                $code .= "\n";
                $code .= '<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>' . "\n";
                $code .= "\n";
                $code .= '<noscript><div style="display:inline;">' . "\n";
                $code .= '<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/' . $this->data['settings']['adwords_conversion_id'] . '/?label=' . $this->data['settings']['adwords_conversion_label'] . '&guid=ON&script=0&value=' . $order_total . '"/>' . "\n";
                $code .= '</div></noscript>' . "\n";
                $code .= "\n";
            }

        } else {
            $code = "";
        }

        return $code;
    }


    /**
     * Returns the generated code snippet
     *
     * @return string
     */
    public function getCode()
    {
        return $this->generateCode();
    }


    /**
     * Returns if the code snippet should be placed at the head or body section
     * Possible return values: body, head
     *
     * @return string
     */
    public function getCodePosition()
    {
        return 'body';
    }


    /**
     * Returns the almighty random number
     *
     * @return int
     */
    public function getRandomNumber()
    {
        return 42;
    }


    /***********************************************************
     * SPEZIELLE SERVICE FUNKTIONEN
     ***********************************************************/


}