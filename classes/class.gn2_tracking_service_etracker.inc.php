<?php
/**
 * GN2_Tracking_Service_ETracker
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage Service_ETracker
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @author     Steve Knornschild <knornschild@sitzdesign.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
class GN2_Tracking_Service_ETracker extends GN2_Tracking_Environment
{

    /**
     * Constructor
     * Set ini settings into class variable
     */
    public function __construct()
    {
        $this->data = parse_ini_file(dirname(__FILE__) . '/../gn2_tracking.ini', true);
    }


    /**
     * main function to generate the snippet
     *
     * @return string
     */
    public function generateCode()
    {
        if (!$this->isBackend()) {
            $code = "";

            if ($this->getAccount() != "") {
                $id = $this->getAccount();

                $code .= '<!-- Copyright (c) 2000-2013 etracker GmbH. All rights reserved. -->'."\n";
                $code .= '<!-- This material may not be reproduced, displayed, modified or distributed -->'."\n";
                $code .= '<!-- without the express prior written permission of the copyright holder. -->'."\n";
                $code .= "\n";

                $code .= '<!-- BEGIN etracker Tracklet 3.0 -->'."\n";
                $code .= '<script type="text/javascript">document.write(String.fromCharCode(60)+\'script type="text/javascript" src="http\'+("https:"==document.location.protocol?"s":"")+\'://code.etracker.com/t.js?et='.$id.'">\'+String.fromCharCode(60)+\'/script>\');</script>'."\n";
                $code .= "\n";

                $code .= '<!-- etracker PARAMETER 3.0 -->'."\n";
                $code .= '<script type="text/javascript">'."\n";

                // Seitenname
                if ($this->getPageview() != "" || $this->isIndex()) {
                    $pageview = $this->getPageview();
                    $prefix = $this->isIndex() ? "__INDEX__" : "";
                    $code .= "\t".'var et_pagename     = "'.$prefix.$pageview.'";'."\n";
                } else {
                    $code .= "\t".'//var et_pagename     = "";'."\n";
                }

                // Bereiche (optional)
                $code .= "\t".'//var et_areas        = "";'."\n";

                // Manuelle URL (optional)
                $code .= "\t".'//var et_url          = "";'."\n";

                // Ziele (Trichter)
                if( $this->getTarget(true) != "" ){
                    $code .= "\t".'var et_target       = "'.$this->getTarget(true).'";'."\n";
                } else {
                    $code .= "\t".'//var et_target       = "";'."\n";
                }

                // Besucherinteresse
                $code .= "\t".'//var et_ilevel       = 0;'."\n";


                if ($this->isShop() && $this->isLastStep()) {
                    // Zeilen an Code anhaengen
                    $code .= $this->getOrderCode(true);
                } else {
                    $code .= "\t".'//var et_tval         = "";'."\n";
                    $code .= "\t".'//var et_tonr         = "";'."\n";
                    $code .= "\t".'//var et_tsale        = 0;'."\n";
                    $code .= "\t".'//var et_cust         = 0;'."\n";
                    $code .= "\t".'//var et_basket       = "";'."\n";
                }


                // Landingpage
                $code .= "\t".'//var et_lpage        = "";'."\n";

                $code .= "\t".'//var et_trig         = "";'."\n";

                // Subkanal
                $code .= "\t".'//var et_sub          = "";'."\n";

                // Suchmaschinen
                $code .= "\t".'//var et_se           = "";'."\n";

                // Frei definierbare Attribute
                $code .= "\t".'//var et_tag          = "";'."\n";

                $code .= '</script>'."\n";
                $code .= '<!-- etracker PARAMETER END -->'."\n";
                $code .= "\n";

                $code .= '<script type="text/javascript">_etc();</script>'."\n";
                $code .= "\n";


                $code .= '<noscript><p><a href="http://www.etracker.com"><img style="border:0px;" alt="" src="https://www.etracker.com/nscnt.php?et='.$id.'" /></a></p></noscript>'."\n";
                $code .= "\n";

                $code .= '<!-- etracker CODE NOSCRIPT 3.0 -->'."\n";
                $code .= '<noscript>'."\n";
                $code .= "\t".'<p><a href="http://www.etracker.de/app?et='.$id.'">'."\n";

                ///////////////////////

                $code .= "\t".'<img style="border:0px;" alt="" src="';

                    $code .= 'https://www.etracker.de/cnt.php?et='.$id.'&amp;v=3.0&amp;java=n&amp;et_easy=0&amp;';

                    // Seitenname
                    if ($this->getPageview() != "" || $this->isIndex()) {
                        $pageview = $this->getPageview();
                        $prefix = $this->isIndex() ? "__INDEX__" : "";
                        $code .= 'et_pagename='.$prefix.$pageview.'&amp;';
                    } else {
                        $code .= 'et_pagename=&amp;';
                    }

                    // Bereiche (optional)
                    $code .= 'et_areas=&amp;';

                    // Manuelle URL (optional)
                    $code .= 'et_url=&amp;';

                    // Ziele (Trichter)
                    if( $this->getTarget(false) != "" ){
                        $code .= 'et_target='.$this->getTarget(false).'&amp;';
                    } else {
                        $code .= 'et_target=,0,0,0&amp;';
                    }

                    // Besucherinteresse
                    $code .= 'et_ilevel=0&amp;';

                    // Landingpage
                    $code .= 'et_lpage=0&amp;';

                    $code .= 'et_trig=0&amp;';

                    // Subkanal
                    $code .= 'et_sub=&amp;';

                    // Suchmaschinen
                    $code .= 'et_se=0&amp;';

                    // Frei definierbare Attribute
                    $code .= 'et_tag=&amp;';

                    if ($this->isShop() && $this->isLastStep()) {
                        // Zeilen an Code anhaengen
                        $code .= $this->getOrderCode(false);
                    } else {
                        $code .= 'et_cust=0&amp;';
                        $code .= 'et_basket=&amp;';
                    }

                    $code .= 'et_organisation=&amp;';
                    $code .= 'et_demographic=';

                $code .= '" /></a></p>'."\n";

                ///////////////////////

                $code .= '</noscript>'."\n";
                $code .= '<!-- etracker CODE NOSCRIPT END-->';
                $code .= "\n";

                $code .= "<!-- etracker CODE END -->";
            }

        } else {
            $code = "";
        }

        return $code;
    }


    /**
     * Returns the generated code snippet
     *
     * @return string
     */
    public function getCode()
    {
        return $this->generateCode();
    }


    /**
     * Returns if the code snippet should be placed at the head or body section
     * Possible return values: body, head
     *
     * @return string
     */
    public function getCodePosition()
    {
        return 'body';
    }


    /**
     * Returns the almighty random number
     *
     * @return int
     */
    public function getRandomNumber()
    {
        return 42;
    }


    /***********************************************************
     * SPEZIELLE SERVICE FUNKTIONEN
     ***********************************************************/


    /**
     * Returns etracker target value used to identify the checkout path
     *
     * @param $mode
     * @return bool|string
     */
    public function getTarget($mode)
    {
        // TODO: Pageview Rückgabewere müssen Environment-Unabhängig sein. In Config oder andere ini-Datei auslagern?
        $cl = $this->getPageview();

        if($mode){
            $sep = "/";
        } else {
            $sep = ",";
        }

        switch ($cl) {

            case "BASKET":
                return 'BASKET';
                break;

            case 'USER_OVERVIEW':
                return 'BASKET'.$sep.'USER_OVERVIEW';
                break;

            case 'USER_NO_REGISTRATION':
                return 'BASKET'.$sep.'USER_NO_REGISTRATION';
                break;

            case 'USER_REGISTRATION':
                return 'BASKET'.$sep.'USER_REGISTRATION';
                break;

            case 'USER_CHANGE_ADDRESS':
                return 'BASKET'.$sep.'USER_CHANGE_ADDRESS';
                break;

            case 'PAYMENT':
                return 'BASKET'.$sep.'USER_CHANGE_ADDRESS'.$sep.'PAYMENT';
                break;

            case 'ORDER_OVERVIEW':
                return 'BASKET'.$sep.'USER_CHANGE_ADDRESS'.$sep.'PAYMENT'.$sep.'ORDER_OVERVIEW';
                break;

            case 'THANK_YOU':
                return 'BASKET'.$sep.'USER_CHANGE_ADDRESS'.$sep.'PAYMENT'.$sep.'ORDER_OVERVIEW'.$sep.'THANK_YOU';
                break;

            default:
                return false;
                break;
        }
    }


    /**
     * Returns the etracker account id
     *
     * @return mixed
     */
    public function getAccount()
    {
        $id = $this->data['settings']['etracker_id'];
        return $id;
    }


    /**
     * Returns the order variables
     *
     * @param $script
     * @return string
     */
    public function getOrderCode($script)
    {
        $order = $this->getOrder();
        $items = $this->getOrderItems();

        $output = "";

        // Umsatz Netto
        if($script){ $output .= "\t".'var et_tval         = "'.$order['OrderTotalNet'].'";'."\n"; }

        // Bestellnummer
        if($script){ $output .= "\t".'var et_tonr         = "'.$order['OrderID'].'";'."\n"; }

        // Bestellstatus - 0=Lead, 1=Sale, 2=Vollstorno
        if($script){ $output .= "\t".'var et_tsale        = 0;'."\n"; }


        // Bestandskunde - 0=Bestandskunde, 1=Neukunde
        $newCust = 0;
        if( $this->isNewCustomer() ){ $newCust = 1; }

        if($script){
            $output .= "\t".'var et_cust         = '.$newCust.';'."\n";
        } else {
            $output .= 'et_cust='.$newCust.'&amp;';
        }


        // Warenkorb
        $basket_output = "";
        $basket_i = 1;
        foreach ($items as $item) {

            foreach($item as $ik=>$iv){
                $iv = str_replace(',', '_', $iv);
                $iv = str_replace(';', '_', $iv);
                $iv = str_replace('"', '_', $iv);
                $iv = str_replace('\'', '_', $iv);
                $item[$ik] = $iv;
            }

            if($basket_i > 1){
                $basket_output .= ";";
            }
            $basket_output .= $item['ItemSKU'].",".$item['ItemName'].",".$item['ItemVariant'].",".$item['ItemQuantity'].",".$item['ItemNetPrice'];

            $basket_i++;
        }

        if($script){
            $output .= "\t".'var et_basket       = "'.$basket_output.'";'."\n";
        } else {
            $output .= 'et_basket='.$basket_output.'&amp;';
        }

        return $output;
    }

}