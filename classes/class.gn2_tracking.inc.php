<?php
/**
 * GN2_Tracking
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage GN2_Tracking
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
class GN2_Tracking
{
    public static $snippets;
    public static $dir;

    public function __construct()
    {
    }


    public static function getGoogleAnalytics()
    {
        $object = false;

        if (class_exists('GN2_Tracking_Environment')) {
            require_once self::$dir . '/classes/class.gn2_tracking_service_googleanalytics.inc.php';
            $object = new GN2_Tracking_Service_GoogleAnalytics;
        }

        return $object;
    }


    public static function getGoogleAdwords()
    {
        $object = false;

        if (class_exists('GN2_Tracking_Environment')) {
            require_once self::$dir . '/classes/class.gn2_tracking_service_googleadwords.inc.php';
            $object = new GN2_Tracking_Service_GoogleAdwords;
        }

        return $object;
    }


    public static function getETracker()
    {
        $object = false;

        if (class_exists('GN2_Tracking_Environment')) {
            require_once self::$dir . '/classes/class.gn2_tracking_service_etracker.inc.php';
            $object = new GN2_Tracking_Service_ETracker;
        }
        return $object;
    }


    static function identifyEnvironment()
    {
        global $REX;

        if (class_exists('oxSuperCfg')) {
            require_once self::$dir . '/classes/class.gn2_tracking_environment_oxid.inc.php';
        } else if (isset($REX)) {
            require_once self::$dir . '/classes/class.gn2_tracking_environment_redaxo.inc.php';
        } else if (class_exists('WP')) {
            require_once self::$dir . '/classes/class.gn2_tracking_environment_wordpress.inc.php';
        }
    }


    public static function generateSnippets()
    {
        $snippets = array();
        $settings = parse_ini_file(dirname(__FILE__) . '/../gn2_tracking.ini', true);
        $services = explode(',', $settings['settings']['services']);

        foreach ($services as $service) {

            $service = trim($service);

            switch ($service) {
                case "google_analytics" :
                    $serviceOutput = self::getGoogleAnalytics();
                    break;
                case "google_adwords" :
                    $serviceOutput = self::getGoogleAdwords();
                    break;
                case "etracker" :
                    $serviceOutput = self::getETracker();
                    break;
                default:
                    $serviceOutput = false;
                    break;
            }

            if (is_object($serviceOutput)) {
                $snippets[] = array(
                    'code' => $serviceOutput->getCode(),
                    'position' => $serviceOutput->getCodePosition(),
                );
            }
        }
        return $snippets;
    }


    public static function replaceOutput($output)
    {
        $snippets = GN2_Tracking::$snippets;

        foreach ($snippets as $snippet) {

            $code = $snippet['code'];

            if ($snippet['position'] == 'head') {
                $positionTag = '</head>';
            } else {
                $positionTag = '</body>';
            }

            $output = str_replace($positionTag, $code.$positionTag, $output);
        }
        return $output;
    }

}
