<?php
/**
 * GN2_Tracking_Environment_Wordpress
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage Environment_Wordpress
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
class GN2_Tracking_Environment
{

    /**
     * Constructor
     */
    public function __construct()
    {

    }


    /**
     * Returns if this cms is a shop system
     *
     * @services general
     * @return bool
     */
    public function isShop()
    {
        return true;
    }


    /**
     * Returns if the actual page is in the cms backend
     *
     * @services general
     * @return bool
     */
    function isBackend()
    {
        // TODO: !!!
        return false;
    }


    /**
     * Returns if the actual page is an unidentifiable index page
     *
     * @services etracker
     * @return bool
     */
    function isIndex()
    {
        return false;
    }


    /**
     * Returns if the actual page is the last order step
     *
     * @services general
     * @return bool
     */
    public function isLastStep()
    {
        return false;
    }


    /**
     * Returns the actual page identifier
     * This can be useful if you clearly need to identify the order steps
     * Common values are: HOME, BASKET, USER_NO_REGISTRATION, USER_REGISTRATION, USER_CHANGE_ADDRESS,
     * USER_OVERVIEW, USER_REGISTRATION, PAYMENT, ORDER_OVERVIEW, THANK_YOU,
     * NEWSLETTER_SUBSCRIBED, NEWSLETTER_UNSUBSCRIBED, NEWSLETTER
     *
     * @services general
     * @return string
     */
    function getPageview()
    {
        return '';
    }


    /**
     * Returns an array with information to the submitted order
     *
     * @services general
     * @return array
     */
    public function getOrder()
    {
        $order = array();
        return $order;
    }


    /**
     * Returns an array with information to the bought articles
     *
     * @services google_analytics, etracker
     * @return array
     */
    public function getOrderItems()
    {
        $items = array();
        return $items;
    }


    /**
     * Checks if the actual order is the first order of this customer
     *
     * @services etracker
     * @return bool
     */
    function isNewCustomer()
    {
        return true;
    }


    /**
     * Returns the name of the used payment method
     *
     * @author Steve Knornschild <knornschild@sitzdesign.de>
     * @services google_analytics
     * @return string
     */
    function getPayment()
    {
        return '';
    }


    /**
     * Checks if the actual page is a shop page to promote product information
     * like a product-detail page, a product list or the basket
     *
     * @services google_adwords
     * @return bool
     */
    function hasProducts()
    {
        return false;
    }


    /**
     * Returns the adwords pagetype for the product tracking
     *
     * @services google_adwords
     * @return string
     */
    function getPagetype()
    {
        return 'other';
    }


    /**
     * Returns an array with product information
     * from products which are promoted on the actual page (list, detail, basket..)
     *
     * @services google_adwords
     * @return array
     */
    function getProducts()
    {
        $products = array();
        return $products;
    }


}
