<?php
/**
 * GN2_Tracking_Environment_Oxid
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage Environment_Oxid
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @author     Steve Knornschild <knornschild@sitzdesign.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
class GN2_Tracking_Environment
{

    /**
     * Constructor
     */
    public function __construct()
    {

    }


    /**
     * Returns if this cms is a shop system
     *
     * @services general
     * @return bool
     */
    function isShop()
    {
        return true;
    }


    /**
     * Returns if the actual page is in the cms backend
     *
     * @services general
     * @return bool
     */
    function isBackend()
    {
        $classes = get_declared_classes();

        if (in_array('oxAdminView', $classes)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Returns if the actual page is an unidentifiable index page
     *
     * @services etracker
     * @return bool
     */
    function isIndex()
    {
        $cl = $this->getOxParam('cl');

        if ($cl === NULL) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Returns if the actual page is the last order step
     *
     * @services general
     * @return bool
     */
    function isLastStep()
    {
        $cl = $this->getOxParam('cl');

        if ($cl == 'thankyou') {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Returns the actual page identifier
     * This can be useful if you clearly need to identify the order steps
     * Common values are: HOME, BASKET, USER_NO_REGISTRATION, USER_REGISTRATION, USER_CHANGE_ADDRESS,
     * USER_OVERVIEW, USER_REGISTRATION, PAYMENT, ORDER_OVERVIEW, THANK_YOU,
     * NEWSLETTER_SUBSCRIBED, NEWSLETTER_UNSUBSCRIBED, NEWSLETTER
     *
     * @services general
     * @return string
     */
    function getPageview()
    {
        $cl = $this->getOxParam('cl');

        switch ($cl) {
            case NULL:
                return 'HOME';
                break;

            case "basket":
                return 'BASKET';
                break;

            case "user":
                $option = $this->getOxParam('option');

                if ($option == "1" && $this->getOxSessionVar('usr') === null) {
                    return 'USER_NO_REGISTRATION';
                } elseif ($option == "3" && $this->getOxSessionVar('usr') === null) {
                    return 'USER_REGISTRATION';
                }
                elseif ($this->getOxSessionVar('usr') !== null) {
                    return 'USER_CHANGE_ADDRESS';
                }
                else {
                    return 'USER_OVERVIEW';
                }
                break;

            case "register":
                return 'USER_REGISTRATION';
                break;

            case "payment":
                return 'PAYMENT';
                break;

            case "order":
                return 'ORDER_OVERVIEW';
                break;

            case "thankyou":
                return 'THANK_YOU';
                break;

            case "newsletter":
                $fnc = $this->getOxParam('fnc');
                if ($fnc == "send") {
                    $status =  $this->getOxParam('subscribeStatus');
                    if ($status == "1") {
                        return "NEWSLETTER_SUBSCRIBED";
                    } else {
                        return "NEWSLETTER_UNSUBSCRIBED";
                    }
                } else {
                    return "NEWSLETTER";
                }
                break;

            default:
                if ($cl != '') {
                    $oView = oxNew($cl);
                    if (is_object($oView)) {
                        return $oView->getTitle();
                    }
                }
                break;
        }
        return '';
    }


    /**
     * Returns an array with information to the submitted order
     *
     * @services general
     * @return array
     */
    function getOrder()
    {
        $order = array();

        $oxOrder = $this->_getOxidOrder();

        // ID
        $order['OrderID'] = $oxOrder->oxorder__oxordernr->value;

        // AFFILIATION
        $order['OrderAffiliation'] = $this->data['settings']['analytics_affiliation'];

        // PRICE TOTAL
        $totalOrderSum = $oxOrder->getTotalOrderSum();
        $order['OrderTotal'] = round($totalOrderSum, 2);

        // PRICE TOTAL NETTO
        $OrderNetSum = $oxOrder->getOrderNetSum();
        $order['OrderTotalNet'] = $OrderNetSum;

        // TAX
        $OrderNetSum = $oxOrder->getOrderNetSum();
        $order['OrderTax'] = round($totalOrderSum - $OrderNetSum, 2);

        // SHIPPING
        $orderShipping = $oxOrder->oxorder__oxdelcost->value;
        $order['OrderShipping'] = round($orderShipping, 2);

        // CITY
        $orderCity = $oxOrder->oxorder__oxdelcity->value;
        if ($orderCity == "") {
            $orderCity = $oxOrder->oxorder__oxbillcity->value;
        }
        $order['OrderCity'] = $orderCity;

        // STATE
        $order['OrderState'] = "";

        // COUNTRY
        $delCountry = $oxOrder->getDelCountry()->value;
        if ($delCountry == "") {
            $delCountry = $oxOrder->getBillCountry()->value;
        }
        $order['OrderCountry'] = $delCountry;

        $salutation = $oxOrder->oxorder__oxbillsal->value;
        $sRetSalutation = "Unbekannt";
        if ($salutation == "MR") {
            $sRetSalutation = "Mann";
        }
        if ($salutation == "MRS") {
            $sRetSalutation = "Frau";
        }
        $order['OrderSalutation'] = $sRetSalutation;

        return $order;
    }


    /**
     * Returns an array with information to the bought articles
     *
     * @services google_analytics, etracker
     * @return array
     */
    function getOrderItems()
    {
        $items = array();
        $oxOrder = $this->_getOxidOrder();

        $oxArticles = $oxOrder->getOrderArticles(true);
        $i = 0;

        foreach ($oxArticles as $oxArticle) {

            // ID
            $items[$i]['OrderID'] = $oxOrder->oxorder__oxordernr->value;

            // SKU - Eindeutige Artikelnummer
            $items[$i]['ItemSKU'] = $oxArticle->oxorderarticles__oxartnum->value;

            // ITEMNAME
            $items[$i]['ItemName'] = $oxArticle->oxorderarticles__oxtitle->value;

            // VARIANT
            $category = '';
            $shopArticle = oxNew('oxarticle');
            $shopArticle->load($oxArticle->oxorderarticles__oxartid->value);
            if (is_object($shopArticle)) {
                $shopCategory = $shopArticle->getCategory();
                if (is_object($shopCategory)) {
                    $oDb = oxDb::getDb();
                    $category = $oDb->getOne('select oxtitle from oxcategories where OXID = "'.$shopCategory->oxcategories__oxid->value.'" LIMIT 1');
                }
            }
            $items[$i]['ItemVariant'] = $category;

            // ITEM PRICE
            $items[$i]['ItemPrice'] = $oxArticle->oxorderarticles__oxbprice->value;

            // ITEM PRICE NETTO
            $items[$i]['ItemNetPrice'] = $oxArticle->oxorderarticles__oxnprice->value;

            // QUANTITY
            $items[$i]['ItemQuantity'] = $oxArticle->oxorderarticles__oxamount->value;

            $i++;
        }

        return $items;
    }


    /**
     * Checks if the actual order is the first order of this customer
     *
     * @services etracker
     * @return bool
     */
    function isNewCustomer()
    {
        $oxOrder = $this->_getOxidOrder();
        $uId = $oxOrder->oxorder__oxuserid->value;

        if($uId != ""){

            $oDb = oxDb::getDb();
            $sSelect = 'SELECT COUNT( * ) FROM  `oxorder` WHERE OXUSERID =  "'.mysql_real_escape_string($uId).'"';
            $count = $oDb->getOne($sSelect);

            if($count > 1){
                return false;
            }
        }

        return true;
    }


    /**
     * Returns the name of the used payment method
     *
     * @author Steve Knornschild <knornschild@sitzdesign.de>
     * @services google_analytics
     * @return string
     */
    function getPayment()
    {
        $oPayment = oxNew('oxpayment');
        $oxOrder = $this->_getOxidOrder();
        $oPayment->loadInLang($oxOrder->getOrderLanguage(), $oxOrder->oxorder__oxpaymenttype->value);
        $payment = $oPayment->oxpayments__oxdesc->value;
        return $payment;
    }


    /**
     * Checks if the actual page is a shop page to promote product information
     * like a product-detail page, a product list or the basket
     *
     * @services google_adwords
     * @return bool
     */
    function hasProducts()
    {
        $cl = $this->getOxParam('cl');

        switch ($cl) {
            case "details" :
            case "alist" :
            case "basket" :
            case "thankyou" :
                return true;
                break;
            default:
                return false;
                break;
        }
    }


    /**
     * Returns the adwords pagetype for the product tracking
     *
     * @services google_adwords
     * @return string
     */
    function getPagetype()
    {
        $cl = $this->getOxParam('cl');

        switch ($cl) {
            case "details" :
                return "product";
                break;
            case "alist" :
                return "category";
                break;
            case "basket" :
                return "cart";
                break;
            case "thankyou":
                return "purchase";
                break;
            case "search":
                return "searchresults";
                break;
            default:
                return 'other';
                break;
        }
    }


    /**
     * Returns an array with product information
     * from products which are promoted on the actual page (list, detail, basket..)
     *
     * @services google_adwords
     * @return array
     */
    function getProducts()
    {
        $cl = $this->getOxParam('cl');

        $products = array();

        // TODO: Suche (case search) mit aufnehmen. Dran denken, dann auch hasProducts() zu erweitern

        switch ($cl) {
            case "details" :
                $oTemp = $this->_getOxidProductDetails();

                $products[] = array(
                    'artNr' => $oTemp->oxarticles__oxartnum->rawValue,
                    'price' => $oTemp->getPrice()->getPrice(),
                );
                break;
            case "alist" :
                $oTemp = $this->_getOxidProductList();

                foreach ($oTemp as $oArticle) {
                    $products[] = array(
                        'artNr' => $oArticle->oxarticles__oxartnum->rawValue,
                        'price' => $oArticle->getPrice()->getPrice(),
                    );
                }
                break;
            case "basket" :
                $oTemp = $this->_getOxidBasket();

                foreach ($oTemp as $oArticle) {
                    $products[] = array(
                        'artNr' => $oArticle->oxarticles__oxartnum->rawValue,
                        'price' => $oArticle->getPrice()->getPrice(),
                    );
                }
                break;
            case "thankyou" :
                $oTemp = $this->getOrderItems();

                foreach ($oTemp as $oArticle) {
                    $products[] = array(
                        'artNr' => $oArticle['ItemSKU'],
                        'price' => $oArticle['ItemPrice'],
                    );
                }
                break;
        }
        return $products;
    }


    /***********************************************************
     * SPEZIELLE ENVIRONMENT FUNKTIONEN
     ***********************************************************/


    /**
     * Returns the product object of the actual details view
     *
     * @services environmental usage only
     * @return mixed
     */
    function _getOxidProductDetails()
    {
        $myConfig = $this->getOxConfig();

        if ($myConfig) {
            $oxProduct = $myConfig->getActiveView()->getProduct();
            return $oxProduct;
        }
        return false;
    }


    /**
     * Returns the product list object of the actual list view
     *
     * @services environmental usage only
     * @return mixed
     */
    function _getOxidProductList()
    {
        $myConfig = $this->getOxConfig();

        if ($myConfig) {
            $oxProducts = $myConfig->getActiveView()->getArticleList();
            return $oxProducts;
        }
        return false;
    }


    /**
     * Returns the basket list object of the actual basket view
     *
     * @services environmental usage only
     * @return bool
     */
    function _getOxidBasket()
    {
        $myConfig = $this->getOxConfig();

        if ($myConfig) {
            $oxProducts = $myConfig->getActiveView()->getBasketArticles();
            return $oxProducts;
        }
        return false;
    }


    /**
     * Returns the oxConfig object - backward compatibility
     *
     * @services environmental usage only
     * @return oxConfig
     */
    function getOxConfig()
    {
        global $myConfig;

        if (!$myConfig) {
            $myConfig = oxRegistry::getConfig();

            if (!$myConfig) {
                $myConfig = oxConfig::getInstance();
            }
        }
        return $myConfig;
    }


    /**
     * Returns oxConfig request parameter
     *
     * @services environmental usage only
     * @param $key
     * @return mixed
     */
    function getOxParam($key)
    {
        $myConfig = $this->getOxConfig();
        $param = $myConfig->getRequestParameter($key);
        return $param;
    }


    /**
     * Returns the oxid session object - backward compatibility
     *
     * @services environmental usage only
     * @return oxSession
     */
    function getOxSession()
    {
        global $mySession;

        if (!$mySession) {
            $mySession = oxRegistry::getSession();

            if (!$mySession) {
                $myConfig = $this->getOxConfig();
                $mySession = $myConfig->getActiveView()->getSession();
            }
        }
        return $mySession;
    }


    /**
     * Get value out of the oxid session
     *
     * @services environmental usage only
     * @param $key
     * @return mixed
     */
    function getOxSessionVar($key)
    {
        $mySession = $this->getOxSession();
        $var = $mySession->getVariable($key);
        return $var;
    }


    /**
     * Get oxid order object - backward compatibility
     *
     * @services environmental usage only
     * @return bool
     */
    function _getOxidOrder()
    {
        $myConfig = $this->getOxConfig();

        if ($myConfig) {
            if (version_compare($myConfig->getVersion(), '4.4.8', '>=')) {
                $oxOrder = $myConfig->getActiveView()->getOrder();
                return $oxOrder;
            } else {
                // TODO: TEST THIS!
                $oxOrder = $this->_tpl_vars['order'];
                return $oxOrder;
            }
        }
        return false;
    }

}