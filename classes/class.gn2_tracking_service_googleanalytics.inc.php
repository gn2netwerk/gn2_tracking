<?php
/**
 * GN2_Tracking_Service_GoogleAnalytics
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage Service_GoogleAnalytics
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @author     Steve Knornschild <knornschild@sitzdesign.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
class GN2_Tracking_Service_GoogleAnalytics extends GN2_Tracking_Environment
{

    /**
     * Constructor
     * Set ini settings into class variable
     */
    public function __construct()
    {
        $this->data = parse_ini_file(dirname(__FILE__) . '/../gn2_tracking.ini', true);
    }


    /**
     * main function to generate the snippet
     *
     * @return string
     */
    public function generateCode()
    {
        if (!$this->isBackend() && $this->getAccount() != "") {
            $code = "";

            $code .= '<script type="text/javascript">' . " \n";

            $code .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){". " \n";
            $code .= "\t" . "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),". " \n";
            $code .= "\t" . "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)". " \n";
            $code .= "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');". " \n";
            $code .= "\n";

            $domainname = 'auto';
            if ($this->getDomainName()) {
                $domainname = $this->getDomainName();
            }

            $id = $this->getAccount();
            $code .= "ga('create', '" . $id . "', '" . $domainname . "'); \n";
            $code .= "ga('set', 'anonymizeIp', true); \n";

            if ($this->getPageview() != "") {
                $pageview = $this->getPageview();
                $code .= "ga('send', 'pageview', '" . $pageview . "'); \n";
            } else {
                $code .= "ga('send', 'pageview'); \n";
            }

            if ($this->isShop() && $this->isLastStep()) {
                // Bestelldaten verarbeiten
                $code .= $this->getOrderCode();
            }

            $code .= "</script> \n";


        } else {
            $code = "";
        }

        return $code;

    }


    /**
     * Returns the generated code snippet
     *
     * @return string
     */
    public function getCode()
    {
        return $this->generateCode();
    }


    /**
     * Returns if the code snippet should be placed at the head or body section
     * Possible return values: body, head
     *
     * @return string
     */
    public function getCodePosition()
    {
        return 'head';
    }


    /**
     * Returns the almighty random number
     *
     * @return int
     */
    public function getRandomNumber()
    {
        return 42;
    }


    /***********************************************************
     * SPEZIELLE SERVICE FUNKTIONEN
     ***********************************************************/


    /**
     * Returns the google analytics account id
     *
     * @return string
     */
    public function getAccount()
    {
        $id = $this->data['settings']['analytics_id'];
        return $id;
    }


    /**
     * Returns the google analytics domain name
     *
     * @return string
     */
    public function getDomainName()
    {
        $domainname = $this->data['settings']['analytics_domainname'];
        return $domainname;
    }


    /**
     * Returns the order variables
     *
     * @return string
     */
    public function getOrderCode()
    {
        $order = $this->getOrder();
        $items = $this->getOrderItems();
        $output = '';

        // GA-Ecommerce Plugin laden
        $output .= "ga('require', 'ecommerce'); \n";
        $output .= "\n";

        // Transaktion loggen
        $output .= "ga('ecommerce:addTransaction', {" . "\n";
        $output .= "\t" . "'id': '" . $order['OrderID'] . "'," . "\n"; // order ID - required
        $output .= "\t" . "'affiliation': '" . $order['OrderAffiliation'] . "'," . "\n"; // affiliation or store name
        $output .= "\t" . "'revenue': '" . $order['OrderTotal'] . "'," . "\n"; // total - required
        $output .= "\t" . "'shipping': '" . $order['OrderShipping'] . "'," . "\n"; // shipping
        $output .= "\t" . "'tax': '" . $order['OrderTax'] . "'," . "\n"; // tax
        //$output .= "\t" . "'" . $order['OrderCity'] . "'," . "\n"; // city
        //$output .= "\t" . "'" . $order['OrderState'] . "'," . "\n"; // state or province
        //$output .= "\t" . "'" . $order['OrderCountry'] . "'" . "\n"; // country
        $output .= "});" . "\n";
        $output .= "\n";

        // Gekaufte Artikel loggen
        foreach ($items as $item) {
            $output .= "ga('ecommerce:addItem', {" . "\n";
            $output .= "\t" . "'id': '" . $item['OrderID'] . "'," . "\n"; // order ID - necessary to associate item with transaction
            $output .= "\t" . "'name': '" . $item['ItemName'] . "'," . "\n"; // product name - necessary to associate revenue with product
            $output .= "\t" . "'sku': '" . $item['ItemSKU'] . "'," . "\n"; // SKU/code - required
            $output .= "\t" . "'category': '" . $item['ItemVariant'] . "'," . "\n"; // category or variation
            $output .= "\t" . "'price': '" . $item['ItemPrice'] . "'," . "\n"; // unit price - required
            $output .= "\t" . "'quantity': '" . $item['ItemQuantity'] . "'" . "\n"; // quantity - required
            $output .= "});" . "\n";
            $output .= "\n";
        }

        // Bestelldaten übermitteln
        $output .= "ga('ecommerce:send'); \n";
        $output .= "\n";

        // Bestellung zusätzlich per Event-Tracking verschicken
        foreach ($items as $item) {
            $sArticleName = $item['ItemName'];
            $sArticleQuantity = $item['ItemQuantity'];
            $output .= "ga('send', 'event', 'Artikel', 'Kaufabschluss', '" . $sArticleName . "', ".$sArticleQuantity."); \n";
        }

        // Payment-Type loggen
        $payment = $this->getPayment();
        $output .= "ga('send', 'event', 'Zahlungsart', 'Kaufabschluss', '" . $payment . "'); \n";

        // Geschlecht des Bestellers loggen
        $gender = $order['OrderSalutation'];
        $output .= "ga('send', 'event', 'Geschlecht', 'Kaufabschluss', '" . $gender . "'); \n";

        return $output;
    }

}