<?php
/**
 * GN2_Tracking_Environment_Redaxo
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage Environment_Redaxo
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
class GN2_Tracking_Environment
{

    // TODO: Lass mal die Redaxo-Klasse neu schreiben. So ein Müll hier.
    // TODO: Es erkennt kein Backend und ist immer automatisch ein Shop, egal ob Rexsale installiert oder nicht.
    // TODO: Das ganze Ding ist Mist. Komm wir schmeißen RexSale raus und setzen es für REX neu auf.


    /**
     * Constructor
     */
    public function __construct()
    {
        if (class_exists('ooRexSaleCart')) {
            $this->basket = new ooRexSaleCart;
            unset($this->basket->config); // cleanup array
        }
        if (class_exists('ooRexSaleUser')) {
            $user = new ooRexSaleUser();
            $userdata = $user->getUserData($user->user['id']);
            $this->user = $userdata[0];

            $this->user['fDEL_COUNTRY'] = (intVal($this->user['rDEL_COUNTRY']) > 0) ? $user->getUserCountry($this->user['rDEL_COUNTRY']) : "";
            $this->user['fBILL_COUNTRY'] = (intVal($this->user['rBILL_COUNTRY']) > 0) ? $user->getUserCountry($this->user['rBILL_COUNTRY']) : "";
        }
    }


    /**
     * Returns if this cms is a shop system
     *
     * @services general
     * @return bool
     */
    public function isShop()
    {
        return true;
    }


    /**
     * Returns if the actual page is in the cms backend
     *
     * @services general
     * @return bool
     */
    function isBackend()
    {
        global $REX;

        if ($REX['REDAXO']) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Returns if the actual page is an unidentifiable index page
     *
     * @services etracker
     * @return bool
     */
    function isIndex()
    {
        return false;
    }


    /**
     * Returns if the actual page is the last order step
     *
     * @services general
     * @return bool
     */
    public function isLastStep()
    {
        global $REX;
        return true;
        // SECOND TO LAST STEP
        #if (....) {
        #	// cookie setzen
        #}

        // LAST STEP
        if ($REX['ADDON']['REXSALE']['MODE'] == 't' && isset($_SESSION['REXSALE_GA'])) {
            return true;
        }
    }


    /**
     * Returns the actual page identifier
     * This can be useful if you clearly need to identify the order steps
     * Common values are: HOME, BASKET, USER_NO_REGISTRATION, USER_REGISTRATION, USER_CHANGE_ADDRESS,
     * USER_OVERVIEW, USER_REGISTRATION, PAYMENT, ORDER_OVERVIEW, THANK_YOU,
     * NEWSLETTER_SUBSCRIBED, NEWSLETTER_UNSUBSCRIBED, NEWSLETTER
     *
     * @services general
     * @return string
     */
    function getPageview()
    {
        return '';
    }


    /**
     * Returns an array with information to the submitted order
     *
     * @services general
     * @return array
     */
    public function getOrder()
    {
        $order = array();

        // ID
        $order['OrderID'] = date("Ymd-His") . '-r' . rand();

        // AFFILIATION
        $order['OrderAffiliation'] = $this->data['settings']['analytics_affiliation'];

        // PRICE TOTAL
        $totalOrderSum = number_format(floatVal($this->basket->total), 2, '.', '');
        $order['OrderTotal'] = $totalOrderSum;

        // TAX
        $orderNetSum = number_format(floatVal($this->basket->includedTax), 2, '.', '');
        $order['OrderTax'] = $orderNetSum;

        // SHIPPING
        $orderShipping = number_format(floatVal($this->basket->postage), 2, '.', '');
        $order['OrderShipping'] = $orderShipping;

        // CITY
        $orderCity = $this->user['fDEL_TOWN'];
        if ($orderCity == "") {
            $orderCity = $this->user['fBILL_TOWN'];
        }
        $order['OrderCity'] = $orderCity;

        // STATE
        $orderState = $this->user['fDEL_STATE'];
        if ($orderState == "") {
            $orderState = $this->user['fBILL_STATE'];
        }
        $order['OrderState'] = $orderState;


        // COUNTRY
        $delCountry = $this->user['fDEL_COUNTRY'];
        if ($delCountry == "") {
            $delCountry = $this->user['fBILL_COUNTRY'];
        }
        $order['OrderCountry'] = $delCountry;
        $this->order = $order;
        return $order;
    }


    /**
     * Returns an array with information to the bought articles
     *
     * @services google_analytics, etracker
     * @return array
     */
    public function getOrderItems()
    {
        $products = $this->basket->items;
        $items = array();
        foreach ($products as $product) {
            // ID
            $item['OrderID'] = $this->order['OrderID'];
            // SKU - Eindeutige Artikelnummer
            $item['ItemSKU'] = md5($product['ident']);
            // ITEMNAME
            $item['ItemName'] = $product['name'];
            // VARIANT
            $variants = '';
            if (is_array($product['variants'])) {
                $variants = implode($product['variants'], ', ');
            }
            $item['ItemVariant'] = $variants;
            // ITEM PRICE
            $item['ItemPrice'] = number_format(floatVal($product['origprice']), 2, '.', '');
            // QUANTITY
            $item['ItemQuantity'] = $product['amount'];

            $items[] = $item;
        }
        return $items;
    }


    /**
     * Checks if the actual order is the first order of this customer
     *
     * @services etracker
     * @return bool
     */
    function isNewCustomer()
    {
        return true;
    }


    /**
     * Returns the name of the used payment method
     *
     * @author Steve Knornschild <knornschild@sitzdesign.de>
     * @services google_analytics
     * @return string
     */
    function getPayment()
    {
        return '';
    }


    /**
     * Checks if the actual page is a shop page to promote product information
     * like a product-detail page, a product list or the basket
     *
     * @services google_adwords
     * @return bool
     */
    function hasProducts()
    {
        return false;
    }


    /**
     * Returns the adwords pagetype for the product tracking
     *
     * @services google_adwords
     * @return string
     */
    function getPagetype()
    {
        return 'other';
    }


    /**
     * Returns an array with product information
     * from products which are promoted on the actual page (list, detail, basket..)
     *
     * @services google_adwords
     * @return array
     */
    function getProducts()
    {
        $products = array();
        return $products;
    }


}
