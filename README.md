gn2_tracking
============

Automatic User, Conversion, E-Commerce, AdWords & Event-Tracking for OXID eShop
-------------------------------------------------------------------------------

gn2 tracking takes away the pain of adding all the Google Snippets to your site. Just upload to /modules/gn2_tracking, add your UA to settings.ini, activate and add your conversion funnel in Google Analytics (see INSTALL.pdf). Done.

*Note 1:* This should work fine in CE/PE. We haven't tested it with subshops (EE).

*Note 2:* Don't install this in a subfolder. It will probably work, but we just hate subfolders so much that we thought it was worth mentioning.


