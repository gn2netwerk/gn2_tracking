<?php
/**
 * GN2_Tracking
 *
 * @category   GN2_Tracking
 * @package    GN2_Tracking
 * @subpackage GN2_Tracking
 * @author     Christoph Stäblein <cs@gn2-netwerk.de>
 * @author     Dave Holloway <dh@gn2-netwerk.de>
 * @license    GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version    Release: <package_version>
 * @link       http://www.gn2-netwerk.de/
 */
/*
* HALLO, SCHOEN DASS DU MAL VORBEISCHAUST! ;)
*/

$dir = $REX['INCLUDE_PATH'] . "/addons/gn2_tracking/";
include_once($dir . "bootstrap.inc.php");

function GN2_Tracking_Output_Filter($params)
{
    global $REX;
    $content = $params['subject'];
    $content = GN2_Tracking::replaceOutput($content);
    return $content;
}

rex_register_extension('OUTPUT_FILTER', 'gn2_tracking_output_filter');
?>